/*
 
 
 _________ ____  .-. _________/ ____ .-. ____ 
 __|__  (_)_/(_)(   )____<    \|    (   )  (_)
                 `-'                 `-'      


 art & game engine

 ____________________________________  ?   ____________________________________
                                     (._.)

 
 This file is part of darkcity package, the city map generator of polymorph
 engine.
 For the latest info, see http://polymorph.cool/
 
 Copyright (c) 2017 polymorph.cool
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 ascii font: rotated by MikeChat & myflix
 have fun and be cool :)
 
 ___________________________________( ^3^)_____________________________________
  
 */

/* 
 * File:   DCMultigon.cpp
 * Author: frankiezafe
 * 
 * Created on April 27, 2017, 7:19 PM
 */

#include "DCMultigon.h"

using namespace darkcity;

template <typename T>
Multigon<T>::Multigon() :
center(_center),
_shape_num(0),
_shapes(0),
_self_intersection(false) {
}

template <typename T>
Multigon<T>::~Multigon() {

    clear();

}

template <typename T>
void Multigon<T>::clear() {

    if (_shapes) {
        for (uint8_t i = 0; i < _shape_num; ++i) {
            delete _shapes[ i ];
        }
        delete [] _shapes;
    }

    _shape_num = 0;
    _shapes = 0;

}

template <typename T>
void Multigon<T>::allocate(uint16_t i) {

    clear();
    _shape_num = 1;
    _shapes = new Polygon<T>*[ 1 ];
    _shapes[0] = new Polygon<T>();
    _shapes[0]->allocate(i);
    link();

}

template <typename T>
void Multigon<T>::allocate(uint8_t shape_num, uint16_t * pps) {

    clear();
    _shape_num = shape_num;
    _shapes = new Polygon<T> * [ _shape_num ];
    for (uint8_t i = 0; i < shape_num; ++i) {
        _shapes[ i ] = new Polygon<T>();
        _shapes[ i ]->allocate(pps[i]);
    }
    link();

}

template <typename T>
void Multigon<T>::operator=(const Multigon& src) {

    clear();
    _shape_num = src.shape_num();
    _shapes = new Polygon<T> * [ _shape_num ];
    for (uint8_t i = 0; i < _shape_num; ++i) {
        _shapes[ i ] = new Polygon<T>();
        (*_shapes[ i ]) = src[i];
    }
    link();
    process();

}

template <typename T>
void Multigon<T>::set(uint16_t num, Vec3<T> * pos) {

    clear();
    allocate(num);
    for (uint16_t i = 0; i < num; ++i) {
        (*_shapes[0])[i] = pos[i];
    }
    link();
    process();

}

template <typename T>
void Multigon<T>::scale(T factor) {

    for (uint16_t s = 0; s < _shape_num; ++s) {
        for (uint16_t i = 0; i < _shapes[s]->point_num; ++i) {
            (*_shapes[s])[i] *= factor;
        }
    }
    process();

}

template <typename T>
void Multigon<T>::translate(Vec3<T> offset) {

    for (uint16_t s = 0; s < _shape_num; ++s) {
        for (uint16_t i = 0; i < _shapes[s]->point_num; ++i) {
            (*_shapes[s])[i] += offset;
        }
    }

}

template <typename T>
void Multigon<T>::link() {

    if (!_shapes) {
        return;
    }

    for (uint8_t s = 0; s < _shape_num; ++s) {
        _shapes[s]->link();
    }

}

template <typename T>
void Multigon<T>::process( bool check_self_intersection ) {

    if (!_shapes) {
        return;
    }

    _center = 0.;

    for (uint8_t s = 0; s < _shape_num; ++s) {

        _shapes[s]->process( check_self_intersection );
        _center += _shapes[s]->center / _shape_num;

        if (_shapes[s]->self_intersection) {
            _self_intersection = true;
        }

    }

}

template <typename T>
void Multigon<T>::events() {

    _self_intersection = false;

    for (uint8_t s = 0; s < _shape_num; ++s) {

        _shapes[s]->events();
        if (_shapes[s]->self_intersection) {
            _self_intersection = true;
        }

    }

}

template <typename T>
T Multigon<T>::shrink(T factor, Multigon<T>& p) {

    if (factor == 0) {
        p = (*this);
        return 0;
    }

    // number of polygon to remove
    uint collapse_polygon_num = 0;
    // delete flag on each polygon
    bool* collapse_polygons = new bool[ _shape_num ];
    // edge event flag on each polygon
    bool* edge_events = new bool[ _shape_num ];

    // number of polygon to add
    uint new_polygon_num = 0;
    // maximum one split at the time, so maximum 2x more polygon in output
    bool* new_polygons = new bool[ _shape_num ];

    // is it possible to apply this factor?
    T possible_factor = factor;
    if (possible_factor < 0) {
        possible_factor *= -1;
    }

    for (uint8_t s = 0; s < _shape_num; ++s) {

        // setting temporary array to default values
        collapse_polygons[ s ] = false;
        edge_events[ s ] = false;
        new_polygons[ s ] = false;

        if (_shapes[s]->point_num == 0) {

            // one less polygon in the output
            ++collapse_polygon_num;
            // polygon will be removed from output
            collapse_polygons[ s ] = true;

        } else {

            if (factor > 0) {

                if (
                        _shapes[s]->edgeevent_in.pt &&
                        possible_factor >= _shapes[s]->edgeevent_in.shrink
                        ) {

                    // edge event occurs on a shape with more than 3 points
                    // dead polygon will be flagged in next pass
                    possible_factor = _shapes[s]->edgeevent_in.shrink;

                }

                if (
                        _shapes[s]->point_num > 4 &&
                        _shapes[s]->splitevent_in_01 &&
                        possible_factor >= _shapes[s]->splitevent_in_01->shrink
                        ) {

                    // split event occurs on a shape with more than 4 points
                    possible_factor = _shapes[s]->splitevent_in_01->shrink;

                }

            } else {

                if (
                        _shapes[s]->edgeevent_out.pt &&
                        possible_factor >= _shapes[s]->edgeevent_out.shrink
                        ) {

                    // edge event occurs on a shape with more than 3 points
                    // dead polygon will be flagged in next pass
                    possible_factor = _shapes[s]->edgeevent_out.shrink;

                }

                if (
                        _shapes[s]->point_num > 4 &&
                        _shapes[s]->splitevent_out_01 &&
                        possible_factor >= _shapes[s]->splitevent_out_01->shrink
                        ) {

                    // split event occurs on a shape with more than 4 points
                    possible_factor = _shapes[s]->splitevent_out_01->shrink;

                }

            }

        }

    }

#ifdef DARKCITY_MULTIGON_DEBUG        
    std::cout << "multigon shrink: " <<
            factor << " > " <<
            possible_factor << " with " <<
            int( _shape_num) << " polygon(s)" << std::endl;
#endif

    // the smallest possible factor has been identify
    // we must now react to this: 
    // if edge event and not enough point, polygon is too tiny to be shrinked
    // if split event, creation of a new polygon 

    if (factor != possible_factor) {

        for (uint8_t s = 0; s < _shape_num; ++s) {

            if (factor > 0) {

                if (
                    _shapes[s]->point_num <= 3 &&
                    _shapes[s]->edgeevent_in.pt &&
                    _shapes[s]->edgeevent_in.shrink <= possible_factor
                    ) {

                    // one less polygon in the output
                    ++collapse_polygon_num;
                    // polygon will be removed from output
                    collapse_polygons[ s ] = true;

                } else if (
                        _shapes[s]->edgeevent_in.pt &&
                        _shapes[s]->edgeevent_in.shrink <= possible_factor
                        ) {

                    // there will be less points in this polygon
                    edge_events[ s ] = true;

                } else if (
                        _shapes[s]->point_num > 4 &&
                        _shapes[s]->splitevent_in_01 &&
                        _shapes[s]->splitevent_in_01->shrink <= possible_factor
                        ) {

                    // one more polygon in the output
                    ++new_polygon_num;
                    // flag for creating an extra polygon for this one
                    new_polygons[ s ] = true;

                }

            } else {

                if (
                    _shapes[s]->point_num <= 3 &&
                    _shapes[s]->edgeevent_out.pt &&
                    _shapes[s]->edgeevent_out.shrink <= possible_factor
                    ) {

                    // one less polygon in the output
                    ++collapse_polygon_num;
                    // polygon will be removed from output
                    collapse_polygons[ s ] = true;

                } else if (
                        _shapes[s]->edgeevent_out.pt &&
                        _shapes[s]->edgeevent_out.shrink <= possible_factor
                        ) {

                    // there will be less points in this polygon
                    edge_events[ s ] = true;

                } else if (
                        _shapes[s]->point_num > 4 &&
                        _shapes[s]->splitevent_out_01 &&
                        _shapes[s]->splitevent_out_01->shrink <= possible_factor
                        ) {

                    // one more polygon in the output
                    ++new_polygon_num;
                    // flag for creating an extra polygon for this one
                    new_polygons[ s ] = true;

                }

            }

        }

    }

    // number of polygons in output might exceed the uint8_t limit!
    uint16_t pn = uint16_t(_shape_num) + new_polygon_num;
    if (
            pn >= 255 || // too many polygons in output!
            pn == 0 // no more polygons in output!
            ) {

#ifdef DARKCITY_MULTIGON_DEBUG        
        std::cout << "multigon shrink error: " <<
                pn << " polygon(s) output, too many or too few" << std::endl;
#endif

        delete [] collapse_polygons;
        delete [] edge_events;
        delete [] new_polygons;
        return 0;

    }

#ifdef DARKCITY_MULTIGON_DEBUG        
    std::cout << "output, new & collapsed: " <<
            int( _shape_num + new_polygon_num) << ", " <<
            int( new_polygon_num) << " & " <<
            int( collapse_polygon_num) << std::endl;
#endif

    // we know now the exact number of polygon to add in the output
    uint8_t p_shape_num = _shape_num + new_polygon_num;
    uint16_t* p_points = new uint16_t[ p_shape_num ];
    // we have to collect the number of points in each one
    for (uint8_t s = 0, t = 0; s < _shape_num; ++s) {

        p_points[ t ] = 0;
        
        if (!collapse_polygons[ s ]) {

            if (factor > 0 && edge_events[ s ]) {

                _shapes[ s ]->edgeevent_in_ptnum(p_points[ t ], possible_factor);

            } else if (factor < 0 && edge_events[ s ]) {

                _shapes[ s ]->edgeevent_out_ptnum(p_points[ t ], possible_factor);

            } else if (factor > 0 && new_polygons[ s ]) {

                uint8_t tt = t + 1;
                _shapes[ s ]->splitevent_in_ptnum(p_points[ t ], p_points[ tt ]);
                ++t;

            } else if (factor < 0 && new_polygons[ s ]) {

                uint8_t tt = t + 1;
                _shapes[ s ]->splitevent_out_ptnum(p_points[ t ], p_points[ tt ]);
                ++t;

            } else {

                p_points[ t ] = _shapes[ s ]->point_num;

            }
            
            // anti-line security
            if ( p_points[ t ] < 3 ) {
                p_points[ t ] = 0;
            }

        }

        ++t;

    }

    // we can now populate the output multigon with the right data

    p.allocate(p_shape_num, p_points);

    if (factor < 0) {
        possible_factor *= -1;
    }

    for (uint8_t s = 0, t = 0; s < _shape_num; ++s) {

        if ( p_points[ t ] == 0 ) {

            _shapes[s]->projected_center( p[t].center );

        } else if (new_polygons[ s ]) {

            uint8_t tt = t + 1;
            _shapes[s]->shrink(possible_factor, &p[t], &p[tt]);
            ++t;

        } else {

            _shapes[s]->shrink(possible_factor, &p[t], 0);

        }

        ++t;

    }

    delete [] p_points;
    delete [] collapse_polygons;
    delete [] edge_events;
    delete [] new_polygons;

    return possible_factor;

}

template <typename T>
void Multigon<T>::recalculate_normal(bool outside) {

}

template <typename T>
void Multigon<T>::resolve_intersection() {

    if (!_self_intersection) {
        return;
    }

    while (_self_intersection) {
        for (uint8_t i = 0; i < _shape_num; ++i) {
            if (_shapes[i]->self_intersection) {
                resolve_intersection(i);
                break;
            }
        }
    }

}

template <typename T>
void Multigon<T>::resolve_intersection(uint8_t s) {

    if (!_shapes[s]->self_intersection) {
        return;
    }

    uint16_t in_id = 0;
    uint16_t out_id = 0;
    Point<T>* in_pt = 0;
    Point<T>* out_pt = 0;

    uint16_t ptn = _shapes[s]->point_num;
    Vec3<T> ee;

    for (uint16_t i = 0; i < ptn; ++i) {
        Point<T>* pi = &(*_shapes[s])[i];
        for (uint16_t o = 0; o < ptn; ++o) {
            Point<T>* po = &(*_shapes[s])[o];
            if (
                    pi == po ||
                    pi == po->next ||
                    pi->next == po ||
                    pi->next == po->next
                    ) {
                continue;
            }
            if (Geometry<T>::intersection(
                    (*pi), (*pi->next),
                    (*po), (*po->next),
                    ee, true, true
                    )
                    ) {
                in_id = i;
                out_id = o;
                in_pt = pi;
                out_pt = po;
                break;
            }

        }
        if (in_pt) {
            break;
        }
    }

    if (!in_pt) {
        return;
    }

    // removing up axis component
    ee[Vec3<T>::upAxis] = 0;

    Point<T>* cp = 0;
    // ready to rock!
    // creating a new point array for split1

    Polygon<T>* split1 = new Polygon<T>();
    split1->allocate((out_id - in_id) + 1);
    cp = in_pt->next;
    for (uint16_t i = 0; i < split1->point_num; ++i) {
        if (i == 0) {
            (*split1)[i] = ee;
        } else {
            (*split1)[i] = (*cp);
            cp = cp->next;
        }
    }
    split1->link();
    split1->process();

    Polygon<T>* split2 = new Polygon<T>();
    split2->allocate((ptn + 1 - (out_id - in_id)));
    cp = out_pt->next;
    for (uint16_t i = 0; i < split2->point_num; ++i) {
        if (i == split2->point_num - 1) {
            (*split2)[i] = ee;
        } else {
            (*split2)[i] = (*cp);
            cp = cp->next;
        }
    }
    split2->link();
    split2->process();
    //    split2->flip_normal();
    //    split2->events();

    // copying the old pointers in a temp array
    uint8_t tmpnum = _shape_num;
    Polygon<T> ** tmppolys = new Polygon<T>*[ _shape_num ];
    for (uint8_t i = 0; i < tmpnum; ++i) {
        if (i == s) {
            delete _shapes[ i ];
            tmppolys[ i ] = 0;
        } else {
            tmppolys[ i ] = _shapes[ i ];
        }
    }
    delete [] _shapes;

    // increasing the array of polygon pointers by one
    ++_shape_num;
    _shapes = new Polygon<T> * [ _shape_num ];
    _self_intersection = false;

    for (uint8_t i = 0; i < _shape_num; ++i) {
        if (i == s) {
            _shapes[ i ] = split1;
        } else if (i == _shape_num - 1) {
            _shapes[ i ] = split2;
        } else {
            _shapes[ i ] = tmppolys[ i ];
        }
        if (_shapes[ i ]->self_intersection) {
            _self_intersection = true;
        }
    }
    delete [] tmppolys;

}

template <typename T>
bool Multigon<T>::closest(
        uint8_t& shape, uint16_t& point,
        const Vec3<T>& v, T range) {

    if (!_shapes) {
        return false;
    }

    bool firstrun = true;
    T closest = 0;

    for (uint8_t s = 0; s < _shape_num; ++s) {

        Point<T>* p = &(*_shapes[s])[0];
        int i = 0;
        while (p) {

            T dist = p->distance(v);

            if (range != -1 && dist > range) {
                p = p->next;
                ++i;
                if (p == &(*_shapes[s])[0]) {
                    break;
                }
                continue;
            }

            if (firstrun || closest > dist) {
                firstrun = false;
                shape = s;
                point = i;
                closest = dist;
            }

            p = p->next;
            ++i;

            if (p == &(*_shapes[s])[0]) {
                break;
            }

        }

    }

    if (firstrun) {
        return false;
    } else {
        return true;
    }

}

template <typename T>
bool Multigon<T>::closest2D(
        uint8_t& shape, 
        uint16_t& point,
        const T value_axis1, 
        const T value_axis2, 
        T range) {

    if (!_shapes) {
        return false;
    }

    bool firstrun = true;
    T closest = 0;

    Vec3<T> tmp;
    tmp[Vec3<T>::axis1] = value_axis1;
    tmp[Vec3<T>::axis2] = value_axis2;
    
    for (uint8_t s = 0; s < _shape_num; ++s) {

        Point<T>* p = &(*_shapes[s])[0];
        int i = 0;
        while (p) {
            
            tmp[Vec3<T>::upAxis] = (*p)[Vec3<T>::upAxis];
            T dist = p->distance(tmp);

            if (range != -1 && dist > range) {
                p = p->next;
                ++i;
                if (p == &(*_shapes[s])[0]) {
                    break;
                }
                continue;
            }

            if (firstrun || closest > dist) {
                firstrun = false;
                shape = s;
                point = i;
                closest = dist;
            }

            p = p->next;
            ++i;

            if (p == &(*_shapes[s])[0]) {
                break;
            }

        }

    }

    if (firstrun) {
        return false;
    } else {
        return true;
    }

}

template class Multigon<float>;
template class Multigon<double>;